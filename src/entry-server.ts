import { createApp } from './main';


export default (context: any) => {
  // since there could potentially be asynchronous route hooks or components,
  // we will be returning a Promise so that the server can wait until
  // everything is ready before rendering.
  return new Promise((resolve, reject) => {
    const { app, router } = createApp();
    const { url } = context;
    const meta = app.$meta();
    // const { fullPath } = router.resolve(url).route;

    // set server-side router's location
    router.push(url);
    // if (router.currentRoute.path !== url) { router.push(url); }

    context.meta = meta;

    // wait until router has resolved possible async components and hooks
    router.onReady(() => {
      const matchedComponents = router.getMatchedComponents();
      // no matched routes, reject with 404
      if (!matchedComponents.length) {
        return reject({ code: 404 });
      }

      // the Promise should resolve to the app instance so it can be rendered
      return resolve(app);
    }, reject);
  });
};
