.PHONY: deploy-master
deploy-master:
	sh deploy/generate_nomad_job.sh master | nomad run -

build:
	docker build --tag pashinin-test .

build-nginx:
	docker build --tag pashinin-test-nginx -f Dockerfile-nginx .

build-node:
	docker build --tag pashinin-test-node -f Dockerfile-node .

# test:
# 	curl --fail http://localhost/contacts
