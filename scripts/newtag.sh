#!/bin/bash


# Get highest tag number. Also cut off "v" symbols if any
VERSION=`git describe --abbrev=0 --tags | sed -e 's/^[v]*//'`

# Replace . with space so can split into an array
VERSION_BITS=(${VERSION//./ })

#get number parts and increase last one by 1
VNUM1=${VERSION_BITS[0]}
VNUM2=${VERSION_BITS[1]}
VNUM3=${VERSION_BITS[2]}
VNUM3=$((VNUM3+1))

# New tag
NEW_TAG="$VNUM1.$VNUM2.$VNUM3"

# get current hash and see if it already has a tag
GIT_COMMIT=`git rev-parse HEAD`
NEEDS_TAG=`git describe --contains $GIT_COMMIT 2>/dev/null`

# only tag if no tag already
if [ -z "$NEEDS_TAG" ]; then
    echo "Updating $VERSION to $NEW_TAG"

    # Creates a commit and a tag with a new version
    # --no-git-tag-version
    yarn version --new-version $NEW_TAG

    # Creates a tag
    # git tag $NEW_TAG

    echo "Tagged with $NEW_TAG"
else
    echo "Already tagged on this commit: $VERSION"
fi

echo "Push tags?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) git push --tags; break;;
        No ) exit;;
    esac
done
