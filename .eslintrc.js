// http://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  // parser: 'babel-eslint',
  parserOptions: {
    // sourceType: 'module',
    parser: '@typescript-eslint/parser',
  },
  env: {
    node: true,
    // browser: true,
    // 'es6': true,
  },
  // https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
  // extends: 'standard',
  // 'extends': 'google',
  // 'extends': 'airbnb-base',  // without react
  // 'extends': 'airbnb',    // for react
  "extends": [
    // 'airbnb-base',  // without react
    "plugin:vue/essential",
    // '@vue/airbnb',
    "eslint:recommended",
    "@vue/typescript",
  ],

  // required to lint *.vue files
  plugins: [
    'html'
  ],
  // add your custom rules here
  'rules': {
    // 'no-alert': 0,
    // allow paren-less arrow functions
    'arrow-parens': 0,

    // allow async-await
    'generator-star-spacing': 0,

    // allow debugger during development
    // 'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  },
  "settings": {
    // "import/resolver": {
    //   "webpack": {
    //     "config": "build/webpack.base.conf.js"
    //   }
    // }
  }
}
